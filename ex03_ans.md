# 演習問題3

## 3.3 シュトラッセンのアルゴリズム

省略
****
## 3.4 Unimodal Search

最大要素A[m]を求める，c言語により記述したアルゴリズムを以下に示す．

```
int UnimodalSearch(int A[], int n){
    int i = 0, j = n-1, s1, s2;
    while (i != j) {
        s1 = (j-i)/2;
        s1 = (j-i)/2 + 1;
        if (A[s1] > A[s2]) j = s1;
        else i = s2;
    }
    return A[i];
}
```
（※アルゴリズムを書けという問題で，実際にプログラム書いたほうがいいのか，日本語書いたほうがいいのかわちょっとわかんなかったので，書きやすい方にしました．）
****
## 3.5 Polynomial multiplication

問題文  
次数 $n$ の記号変数 $xの多項式は、係数の配列 $P[0 ... n]$ として表現することができる。$A(x) = a_1x + a0 と B(x) = b_1x + b_0$ という二つの線形多項式を考える。$a_1, a_0, b_1, b_0$ はそれぞれ数値係数で、配列 $[a_0, a_1]$ と $[b0, b1]$ で表現することができる。 $A$ と $B$ の掛け算は4係数の掛け算を使えばよい
$$
m_1 = a_1 * b_1\\
m_2 = a_1 * b_0\\
m_3 = a_0 * b_1\\
m_4 = a_0 * b_0\\
$$
という4つの係数の乗算と、1つの数値加算を行い、多項式  
$$
C(x) = m_1x^2 + (m_2 + m_3)x + m_4
$$
という多項式を構成し、これを配列で表すと
$$
[c_0, c_1, c_2] = [m_4, m_3 + m_2, m_1]
$$
となる。


<div style="page-break-before:always"></div>

### (a)  

<!-- 
n-1のA(x)を定義
半分にしたA1(x)を定義
それらの積と和を利用して求めたい積を算出．
-->

問題
係数配列で表される次数 $n-1$ の2つの多項式の乗算を、この式に基づいて分割統治するアルゴリズムを示せ。なお，簡単のため $n$ は2の累乗と仮定してよい．

回答  
$A(x),B(x)$ を $n-1$ の多項式とすると，
$$
\begin{align*}
A(x) &= a_0 + a_1x + a_2x^2 + ... + a_{n-1}x^{n-1}
B(x) &= b_0 + b_1x + b_2x^2 + ... + b_{n-1}x^{n-1}
\end{align*}
$$
次に， $A(x),B(x)$ を次のように半分の項数になるよう分割する．（分割）
$$
\begin{align*}
A(x) &= A_0(x) + A_1(x) x^{n/2}\\
B(x) &= B_0(x) + B_1(x) x^{n/2}
\end{align*}
$$
したがって，$A(x)$ と $B(x)$ の乗算は，次のように表せる（結合）．
$$
A(x)B(x) = A_0(x)B_0(x) + \{A_0(x)B_1(x) + A_1(x)B_0(x)\}x^{n/2} + A_1(x)B_1(x)x^n
$$
以上のように再帰的に多項式の積を計算する（統治）．

疑似コードを以下に示す．初期呼び出しは，Multiplication(A,B,n)である．
```
procedure Multiplication(A,B,n)  
    if AとBの配列の要素数が1なら: return [A[0]*B[0]] // 配列として返す．
    A0,A1,B0,B1 = (AとBをそれぞれ部分配列A0,A1,B0,B1に分割する)
    m4 = Multiplication(A1,B1,n/2) 
    m3 = Multiplication(A0,B1,n/2) 
    m2 = Multiplication(A1,B0,n/2) 
    m1 = Multiplication(A0,B0,n/2) 
    return m4~m1を合成した係数配列 m1+(m2+m3)x^n/2 +m4x^n
```
    
### (b) 作成したアルゴリズムの最悪実行時間に関する漸近式を与え，それを解け．

漸化式は，
$$
T(n) = 
    \begin{cases}
        {\Theta(1) \qquad\qquad\quad ({\rm if}~ n = 1)}\\
        {4T(n/2)+\Theta(n) ~ ({\rm if}~ n = 2^k ~ {\rm for}~ k \ge 1)}
    \end{cases}
$$
また，$n^{\lg_{2}{4}} = n^2 = O(n^2)$であるので，
したがってマスター法のケース1より，$T(n)=\Theta(n^2)$

### (c）

問題  
2つの線形多項式 A(x)=a1x＋a0 と B(x)=b1x＋b0 を3つの係数乗算のみで乗算する方法を示せ。

$z_0 = (a_0 + a_1)(b_0 + b_1),~ z_1 = a_0 b_0,~ z_2 = a_1 b_1$ を計算する．すると乗算後の係数行列は次のように表せる．
$$
[z1,~z_0 - z_1 - z_2,~z_2]
$$

### (d) 次数 n-1 の2つの多項式の乗算を、(c)の式に基づいて分割統治するアルゴリズムを示せ。

求めるアルゴリズムの疑似コードを示す．
```
procedure Multiplication(A,B,n)  
    if AとBの配列の要素数が1なら: return [A*B] // 配列として返す．
    A0,A1,B0,B1 = (AとBをそれぞれ部分配列A0,A1,B0,B1に分割する)
    m2 = Multiplication(A0+A1,B0+B1,n/2) 
    m1 = Multiplication(A0,B0,n/2) 
    m0 = Multiplication(A1,B1,n/2) 
    return m4~m1を合成した係数配列 z1+(z0-z1-z2)x^n/2 +m2x^n
```

### (e) このアルゴリズムの最悪実行時間に関する漸化式を与えて解け．

漸化式は，
$$
T(n) = 
    \begin{cases}
        {\Theta(1) \qquad\qquad\quad ({\rm if}~ n = 1)}\\
        {3T(n/2)+\Theta(n) ~ ({\rm if}~ n = 2^k ~ {\rm for}~ k \ge 1)}
    \end{cases}
    $$
また，$n^{\lg_{2}{3}} = O(n^{\lg_{2}3})$ であるので，したがってマスター法のケース1より，$T(n)=\Theta(n^{\lg_{2}3})$

