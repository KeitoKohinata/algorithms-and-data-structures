# 演習問題  2

<!-- ## 2.1

問題  
次の計算式（ $n$ の関数）をオーダー表記（ $O$ 表記）せよ．ただし，なるべく簡略で精密な形にせよ

回答


---

## 2.2

問題  
次の計算式（ $n$ の関数）をオーダー表記（ $O$ 表記）せよ．ただし，なるべく簡略で精密な形にせよ．

回答


---

## 2.3

問題  
以下の問に，理由をつけて Yes/No で答えよ

回答


---

## 2.4

問題  
以下の各式について，正しいか否か？

回答


---

## 2.5

問題  
$$
\log (n!) = \Theta(n\log n)
$$
であることを示せ．

回答


---

## 2.6

問題  
調和数 $H$ は，自然数 $k$ に対して， 
$$H(k)=\Sigma^k_{i=1}(1/i) = 1+1/2+\cdot\cdot\cdot+1/k$$
と定義される．この時，
$$
H(n) = O(\ln n)
$$
であることを示せ．なお，ここでの $\ln x$ は自然対数である．[ヒント： $H(n)$ と $\int^n_1(1/x)dx$ の大小関係，あるいは， $1/i$ と $S_i = \int^i_{i-1}(1/x)dx$ の大小関係を考えよ．]

回答


---

## 2.7

問題（英語）  
For each of the following statements, decide whether it is always true, never true, or sometimes true for asymptotically nonnegative functions $f$ and $g$ . If it is always true or never true, explain why. If it is sometimes true, give one example for it is true, and one for which it is false.

問題（和訳）  
次の漸近的な正の関数 $f,g$ について，常に真であるか，決して真でないか，時々真であるか判断せよ．また，常に真である場合や決して真でない場合には理由を，時々真である場合には，真である場合と偽である場合の例をそれぞれ挙げよ。

回答


---

## 2.8

問題（英語）  
Give asymptotic upper and lower bounds for $T(n)$ in each of the following recurrences. Assume that $T(n)$ is constant for $n ≤ 10$. Make your bounds as tight as possible, and justify your answers.

問題（和訳）  
以下の各回帰における $T(n)$ の漸近上界と下界を与えよ．ただし，$T(n)$は$n ≤ 10$で一定であるとする．また，できるだけ厳密な下界を設定し，その答えの正当性を証明せよ。

回答  

---

## 2.9

問題（英語）  
Prove or disprove each of the following properties related to asymptotic notation. In each of the following assume that $f, g$ and $h$ are asymptotically nonnegative functions.

問題（和訳）  
漸近表記に関する次の各特性を証明または反証せよ．以下の各項目において，$f,g,h$ は漸近的に非負の関数であるとする。

回答


--- -->

## 2.10

問題    
次の関数を増加のオーダーによって順序付けよ．すなわち， $g_1 = \Omega(g_2), g_2 = \Omega(g_3), ... , g_{25}=\Omega(g_{26})$ を満たす関数の配列 $g_1, g_2,...,g_{25}$ を求めよ． $f(n)$ と $g(n)$ が同じクラスにあるための必要純分条件は $f(n)=\Theta(g(n))$ が成り立つことであるとして，得られたリストを同値類に分割せよ．

回答  
次のような順番になる．
$$
\begin{align}
g_{1} &= 2^{2^{n+1}}\\
g_{2} &= 2^{2^n}\\
g_{3} &= (n+1)!\\
g_{4} &= n!\\
g_{5} &= e^n\\
g_{6} &= n\cdot 2^n \\
g_{7} &= 2^n\\
g_{8} &= \left(\frac{3}{2}\right)^n\\
g_{9} &= n^{\lg\lg n}\\
g_{10} &= (\lg n)^{\lg n} = n^{\lg\lg n}\\
g_{11} &= (\lg n)!\\
g_{12} &= n^3\\
g_{13} &= n^2\\
g_{14} &= 4^{\lg n} = n^2\\
g_{15} &= n \lg n\\
g_{16} &= \lg (n!) = \Theta(n \lg n)\\ 
g_{17} &= n\\
g_{18} &= 2^{\lg n} = n\\
g_{19} &= \sqrt{2}^{\lg n} = \sqrt{n}\\
g_{20} &= 2^{\sqrt{2 \lg n}}\\
g_{21} &= \lg ^2 n\\
g_{22} &= \ln n \\
g_{23} &= \sqrt{\lg n} \\
g_{24} &= \ln \ln n\\
g_{25} &= n^{1/\lg n} = 2\\
g_{26} &= 1\\
\end{align}\\
$$

同値類は，
$$
\begin{align*}
[g_{9}] &= \{g_{9}, g_{10}\}\\
[g_{13}] &= \{g_{13}, g_{14}\}\\
[g_{15}] &= \{g_{15}, g_{16}\}\\
[g_{17}] &= \{g_{17}, g_{18}\}\\
[g_{25}] &= \{g_{25}, g_{26}\}\\
\end{align*}
$$
上記で要素に踏まれていない $g_i$ ( $i$ は 1 $\le i \le$ 26 を満たす整数)の同値類は，
$$
[g_i] = \{g_i\}
$$

補足

オーダーでの関数の大小比較をする時，次のようにすると分かりやすいと思います．
- $\lg g_i$ を計算して $n \rightarrow \infty$ とした場合の値を比較する．
- $dg_i/dn$ を計算して $n \rightarrow \infty$ とした場合の値を比較する．
- $d\lg(g_i)/dn$ を計算して $n \rightarrow \infty$ とした場合の値を比較する． 

など

| 変換 | $a^n$ | $n^a$ | $a^{b\sqrt{\lg n}}$ | $(\lg n)^a$
|:-:| :-: | :-: | :-: | :-: |
| 対数変換 | $n\lg a$ | $a\lg n$ | $b\lg a \sqrt{\lg n}$ | $a\lg\lg n$

$g_4$ はスターリングの公式

$$
\begin{align*}
g_4 &= n! \approx \sqrt{2\pi n}\left(\frac{n}{e}\right)^n\\
~\\
g_{11} &= (\lg n)! \\
&\approx \sqrt{2\pi\lg n} \left(\frac{\lg n}{e}\right)^{\lg n} \\
&\approx \sqrt{2\pi}(\lg n)^{\lg n + 1/2}n^{-\lg e}\\
&\approx \sqrt{2\pi \lg n} n^{\lg \lg n - \lg e} \\
~\\
g_{16} &= \lg (n!) \approx \lg\left(\sqrt{2\pi n}\left(\frac{n}{e}\right)^n\right) \\
&\approx \frac{1}{2}\lg (2\pi n) + n \lg \frac{n}{e} = \Theta(n\lg n)\\
~\\
\end{align*}
$$

