# 演習問題4

## 4.1

問題  
クイックソートの最悪ケースでの計算時間が$\Theta(n^2)$になることを，厳密に示せ．つまり，漸化式
$$
T(n) = T(n − 1) + \Theta(n)\\
$$
の解が $T(n) = \Theta(n^2)$ になることを，置き換え法により示せ．

回答  
$(i)$  $T(1) = T(0) + \Theta(1) = \Theta(1)$ である  
$(ii)$ $1 \le k < n$ を満たす整数 $k$ において， $T(k) \le c_1k^2$ となると仮定すると，
$$
\begin{align*}
T(n) &= T(n-1) + \Theta(n)\\
&= c_1(n-1)^2+ \Theta(n)\\
&= c_1n^2 - (2c_1n - c_1 - \Theta(n))
\end{align*}
$$
$c_1$ を十分大きく取れば，$T(n) \le c_1n^2$  
したがって $T(n) = O(n^2)$

<!-- 
- 帰納法の仮定に反しないようにcの値を設定する必要がある．その結果orderやomegaで表せることができたなら嬉しい． 
- Oのときは，残余分が小さいことを示す
- Omegaのときは，残余分が大きいことを示す．
-->

$(iii)$ $1 \le k < n$ を満たす整数 $k$ において， $T(k) \ge c_1 k^2$ となると仮定すると，
$$
\begin{align*}
T(n) &= T(n-1) + \Theta(n)\\
&= c_1 n^2 + (-2c_1n + c_1 + \Theta(n)) 
\end{align*}
$$
$c_1$ を十分小さく取れば，$T(n) \ge c_1n^2$   
したがって $T(n) = \Omega(n^2)$

$\therefore(i),(ii),(iii)$ より，$T(n) = \Theta(n)$

---

## 4.2 

問題  
クイックソートで、降順（つまり、大きいものから順に）にソートするには、クイックソートをどのように変更すればよいか？

回答  
分割において，ピボットの左側の部分配列に $\ge x$ となる要素を配置し，右側の部分配列に $\le x$ となる要素を配置するよう処理を変更する．

---

<!-- 
- ソートというものは基本的に昇順にソートするもの考える
- 問題に出てくる「クイックソート」は，第4回講義資料9ページに出てくる
- 通常クイックソートのピボットは配列の先頭要素
- T(n) = Θ(1) は暗黙に仮定していい
- 挿入ソートは第1回講義資料8ページ
-->

## 4.3

### (a)

問題  
入力の配列 $A$ が昇順にソート済みであるとき， Quicksort の計算時間を求めよ．同じ $A$ に対する Insertion-Sort の計算時間はどうなるか？

回答  
$A$ はソート済みであるので，1度分割したあとの左側部分配列の要素数は 0 ，右側部分配列の要素数は $n-1$ になる．よって $A$ の要素数を $n$ としたときの Quicksort の計算時間 $T_Q(n)$ は
$$
\begin{align*}
T_Q(n) &= T_Q(0) + T_Q(n-1) + \Theta(n)\\
&= \Theta(1) + T_Q(n-1) + \Theta(n)\\
&= T_Q(n-1) + \Theta(n)\\
&= \Theta(n^2)
\end{align*}
$$

また Insertion-Sort では，key とソート済み左部分配列を比較する処理で，keyの比較回数は常に 1 となる．よって Insertion-Sort の計算時間 $T_I(n)$ は，
$$
\begin{align*}
T_I(n) &= T(n-1) + \Theta(1)\\
 &= \Theta(n)
\end{align*}
$$

### (b)

問題  
入力の配列 $A$ が降順にソート済みであるとき，Quicksort の計算時間を求めよ．同じ $A$ に対する Insertion-Sort の計算時間はどうなるか？

回答  
Aは降順にソート済みであるので，1度分割したあとの左側部分配列の要素数は $n-1$ ，右側部分配列の要素数は 0 になる．よって Quicksort の計算時間 $T_Q(n)$ は
$$
\begin{align*}
T_Q(n) &= T_Q(n-1) + T_Q(0) + \Theta(n)\\
&= T_Q(n-1) + \Theta(0) + \Theta(n)\\
&= T_Q(n-1) + \Theta(n)\\
&= \Theta(n^2)
\end{align*}
$$

また Insertion-Sort では，key とソート済み左部分配列(要素数$k-1$)を比較する処理で，keyの比較回数は常に $k-1$ となる．よって Insertion-Sort の計算時間 $T_I(n)$ は，
$$
\begin{align*}
T_I(n) &= T(n-1) + \Theta(n-1)\\
 &= T(n-1) + \Theta(n)\\
 &= \Theta(n^2)
\end{align*}
$$

---

## 4.4

問題  
クイックソートの各レベルにおける分割数は $1〜(1-\alpha)$ の割合であるとする（ $0 < \alpha \le 1/2$ は定数）．再帰木の葉の最小深さはおよそ $-\lg n / \lg \alpha$ であり，最大深さはおよそ $-\lg n / \lg(1 - \alpha)$ であることを示せ．(整数の丸め誤差は気にしなくてよい）．

回答  
最小の深さは常に分割の小さい部分を取る経路となるので，最小の深さを $h_{min}$ とすると，
$$
\begin{align*}
{\alpha}^{h_{min}} n &= 1\\
h_{min} &= \log_{\alpha} \frac{1}{n}\\
  &= \frac{\lg (\frac{1}{n})}{\lg {\alpha}} \\
  &= -\frac{\lg n}{\lg {\alpha}} \\
\end{align*}
$$

最大の深さは常に分割の大きい部分を取る経路となるので，最小の深さを $h_{max}$ とすると，
$$
\begin{align*}
(1-\alpha)^{h_{max}} n &= 1\\
h_{max} &= \log_{1-\alpha} \frac{1}{n}\\
  &= \frac{\lg (\frac{1}{n})}{\lg (1-\alpha)} \\
  &= -\frac{\lg n}{\lg (1-\alpha)} \\
\end{align*}
$$

---

## 4.5

問題  
次の不等式を証明せよ
$$
\Sigma^{n-1}_{k=2} k \lg k \le \frac{1}{2}n^2 \lg n - \frac{1}{8} n^2
$$

回答  
$$
\begin{align*}
\Sigma^{n-1}_{k=2} k\lg k &\le \int^{n}_{1} x \lg x~dx \\
&\le \left[ \frac{1}{2}x^2 \lg x \right] ^n_1 - \frac{1}{2\ln 2}\int^n_1 x~dx\\
&\le \frac{1}{2}n^2 \lg n - \frac{1}{4}\left[x^2\right]^n_1\\
&\le \frac{1}{2}n^2\lg n - \frac{1}{4}n^2 + \frac{1}{4}\\
&\le \frac{1}{2}n^2\lg n - \frac{1}{8}n^2 - \frac{1}{8}n^2 + \frac{1}{4}\\
\end{align*}
$$
$n \ge 3$ という条件で， 
$$
- \frac{1}{8}n^2 + \frac{1}{4} \le 0
$$
$$
\therefore \Sigma^{n-1}_{k=2} k\lg k \le \frac{1}{2}n^2\lg n - \frac{1}{8}n^2
$$

