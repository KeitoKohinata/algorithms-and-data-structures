# 演習問題8

## 8.1

**問題文**  
データ $5, 28, 19, 15, 20, 33, 12, 17, 10$ をこの順にハッシュ表に挿入する．ハッシュ表のサイズを $m =9$ としてチェイン法を用いる場合に，データはどのような配置になるか．ハッシュ関数は $h(k) = k \mod m$ とせよ

**回答**  
連結リストのスタート s は，入力される値と同一の値とならない何らかの値が入力されているものとする．
| ハッシュ表 | 連結リスト | 
|:-:|:-|
|0| s |
|1| s -> 28 -> 19 -> 10 |
|2| s -> 2 |
|3| s -> 12 |
|4| s |
|5| s -> 5 |
|6| s -> 15 -> 27 |
|7| s |
|8| s -> 17 |

---

## 8.2

**問題文**  
データ 10, 22, 31, 4, 15, 28, 17, 88, 59 をこの順にハ
ッシュ表に挿入する．ハッシュ表のサイズを $m =
11$ としてオープンアドレス指定法を用いる場合に，
データはどのような配置になるか．主ハッシュ関数
は $h_1(k) = k \mod m$ とせよ．線形探査を用いた場
合と $h_2(k) = 1 + (k \mod (m − 1))$ とするダブル
ハッシュ法を用いた場合のそれぞれについて示せ．

**回答**    
線形走査の場合，
$$
\begin{align*}
  h(k,i) &= (h_1(k)+i)\mod m \\
  &= (k\mod m +i)\mod m \\
  &= (k+i)\mod m
\end{align*}
$$
| ハッシュ表 | キー |
| :-: | :-: |
|0 | 22 |
|1 | 88 |
|2 |  |
|3 |  |
|4 | 4 |
|5 | 15 |
|6 | 17 |
|7 | 28 |
|8 | 59 |
|9 | 31 |
|10| 10 |

ダブルハッシュ法の場合,
$$
\begin{align*}
  h(k,i) &= (h_1(k)+i\cdot h_2(k))\mod m\\
  &= (k\mod m + i+i(k\mod(m-1))) \mod m
  &= (k+i+i(k\mod(m-1))) \mod m
\end{align*}
$$
| ハッシュ表 | キー |
| :-: | :-: |
|0 | 22 |
|1 |   |
|2 | 59 |
|3 | 17 |
|4 | 4 |
|5 | 15 |
|6 | 28 |
|7 | 88 |
|8 |  |
|9 | 31 |
|10| 10 |


---

## 8.3

**問題文**  
$n$ 個のレコードに対してサイズ $m$ のハッシュ表を用いるハッシュ法を考える．キー空間である普遍集合 $U$ について $|U| > nm$ とする．このとき，どのようなハッシュ関数を用いても，全データが同じスロットにハッシュされるような（そのため，チェイン法での最悪探索時間が $\Theta(n)$ となるような） $U$ の部分集合が存在することを示せ．


**回答**    
（※ U の部分集合 S があった時，「S はどんなハッシュ関数に対しても 1 つのスロットにハッシュされる」と言っているのではなく，「存在するすべてのハッシュ関数それぞれに対して S が存在する」ということを言っている．また，「どのキーも普遍集合 U の要素であり，全て異なる」と講義資料に書いてあり，暗黙的にすべての入力が異なる値を持つことがこの問題でも仮定されていると考えられる（そうじゃなきゃ簡単すぎるが，正直書いてほしい）．）

$m-1$ 個のスロットのそれぞれにおおくとも $n-1$ 個のキーがハッシュされるとき，残りの1個のスロットにハッシュされるキーの数は，
$$
\begin{align*}
  |U|-(n-1)(m-1) > nm-(n-1)(m-1)=n+m-1\ge n
\end{align*}
$$
となる．よって，どのようなハッシュ関数を用いても，全データが同じスロットにハッシュされるような $U$ の部分集合が存在する．

---

## 8.4

### (a)

**問題文**  
チェイン法を用いたハッシングにおける DELETE$(S, x)$ のアルゴリズムを考えよ

**回答**    
```
DELETE(S,x)
  now ← Table[h(x)]
  while now->next != NIL
    if now->next->key = x
      now->next ← now->next->next
      return True
    now ← now->next
  return False
```

### (b)

**問題文**
オープンアドレス指定法を用いたハッシュ法における DELETE$(S, x)$ のアルゴリズムを考えよ．講義で示した INSERT，SEARCH と併用した場合，$x$ が入っていた $S$ のスロットを空にするだけでは問題が生じうることを示せ．

**回答**  
```
DELETE(S,x)
  i ← 0
  while (s ← Table[h(x,i)]) != NIL
    if s.key = x
      s ← NIL
      return True
    i ← i+1
    if i = m
      break
  return False
```
衝突が必ず発生するようなINSERTが複数回実行されたとする．次に，最後に挿入されたキー以外をDELETEで取り除く．ここで，最後に挿入されたキーをSEARCHにより探索しようとした時，途中で空スロットが見つかり，そこで処理を終了するという問題が発生する．


---

## 8.5

**問題文**  
$n \le \frac{m}{2}$ に対して，サイズ $m$ のハッシュ表が $n$ 個の要素を格納するのに用いられるとする．ただし，オープンアドレス指定法により衝突を解決する．

### (a)

**問題文**  
一様ハッシュを仮定する．$i = 1, 2, \dots , n$ に対して， $i$ 番目の挿入が $k + 1$ 回以上の探査を必要とする確率は $2^{−k}$ で抑えられることを示せ．

**回答**  
$i$ 番目の挿入が $k+1$ 回以上の探査を必要とする確率 $P$ は，
$$
\begin{align*}
  P=\left(1+\frac{i-(k+1)}{m-k}\left(\cdots\left(1+\frac{1}{m-(i-2)}\right)\right)\cdots\right)\prod_{j=0}^{k-1}\frac{i-(j+1)}{m-j}
\end{align*}
$$
上式が最大となるのは， $i=n=\frac{m}{2}$ のときなので，
$$
\begin{align*}
  P \le \left(1+\frac{\frac{m}{2}-(k+1)}{m-k}\left(\cdots\left(1+\frac{1}{m-(\frac{m}{2}-2)}\right)\right)\cdots\right)\prod_{j=0}^{k-1}\frac{\frac{m}{2}-(j+1)}{m-j}
\end{align*}
$$
ここで，
$$
\begin{align*}
  \frac{1}{2} \gt \frac{\frac{m}{2}-1-a}{m-a} ~~~~(0<a<m)
\end{align*}
$$
である． $b =\frac{m}{2}-(k+1)-1$ とすると
$$
\begin{align*}
  P &\le \frac{1}{2^{k}}\left(1+\frac{1}{2}\left(\cdots\left(1+\frac{1}{2}\right)\right)\cdots\right)\\
  &\le \frac{1}{2^{k}}\sum_{j=0}^{b}\frac{1}{2^j}\\
  &=\frac{1}{2^{k}}\frac{1-\frac{1}{2^{b+1}}}{1-\frac{1}{2}}\\
  &\le \frac{1}{2^{k-1}}\left(1-\frac{1}{2^{b+1}}\right)\\
  &\le \frac{1}{2^{k-1}}\left(1-\frac{1}{2^{\frac{m}{2}-k-1}}\right)\\
  &\le 2^{-k+1}-2^{-\frac{m}{2}+1} \\
  &\le 2^{-k+1}
\end{align*}
$$
<!-- したがって，$i$ 番目の挿入が $k + 1$ 回以上の探査を必要とする確率は $2^{−k}$ で抑えられる． -->


### (b)

**問題文**  
$i = 1, 2, · · · , n$ に対して，$i$ 番目の挿入が $2 \lg n$回以上の探査を必要とする確率は $1/n^2$ で抑えられることを示せ．

**回答**  
(a)以降が解けていないので無理
<!-- （下の解き方でうまく行くと思ったんだけど，うまく行かなかった．$2\lg n$ ではなく， $2\lg+1$ であってほしい） -->

<!-- $2\lg n$ 回以上の探査を必要とする確率 $P_1$ は， $\lceil\lg n^2\rceil$ 回以上の探査を必要とする確率を $P_2$ としたとき， $P_1 \le P_2$ である． $P_2$ の場合は，前問における，  
\begin{align*}
  k&=\lceil\lg n^2\rceil - 1
\end{align*}
$$
であるから，
$$
\begin{align*}
  P_1 &\le 2^{-\lceil\lg n^2\rceil+1}\\
  &\le \frac{1}{2n^2}\\
  &\le \frac{1}{n^2}
\end{align*}
$$
したがって，$i$ 番目の挿入が $2 \lg n$回以上の探査を必要とする確率は $1/n^2$ で抑えられる．
-->
<!-- $2\lg n=\lg n^2$ 回以上の探査を必要とする確率は，前問における， 
$$
\begin{align*}
  k&=\lg n^2 - 1
\end{align*}
$$
であるから，
$$
\begin{align*}
  P_1 &\le 2^{-\lg n^2+1}\\
  &\le \frac{2}{n^2}\\
  あらら
\end{align*}
$$

※もし $\lg n^2+1$ なら解ける．
したがって，$i$ 番目の挿入が $2 \lg n$回以上の探査を必要とする確率は $1/n^2$ で抑えられる． -->


### (c)

$i$ 番目の挿入に必要な探査回数を確率変数 $X_i$ で表す．問 (b) では $Pr\{X_i > 2 \lg n\} \le 1/n^2$であることを証明することが問われた．確率変数 $X$ を $n$ 回の挿入に現れる探査回数の最大値，すなわち $X = \max_{1\le i\le n} X_i$ と定義する．

**問題文**  
$Pr\{X\gt 2\lg n\} \le 1/n$ を示せ．

**回答**  
(a)以降が解けていないので無理

### (d)

**問題文**  
最長探査列の長さの期待値は， $E[X] = O(\lg n)$ であることを示せ．

**回答**  
(a)以降が解けていないので無理


---

## 8.6

**問題文**  
チェイン法を用いたハッシングにおいて，単純一様ハッシュを仮定すれば， SEARCH に要する時間（期待値）は，$\Theta(1 + \alpha)$ になるということであった．同じ仮定のもと， INSERT や DELETE に要する時間はどうなるか？

**回答**  
INSERT は， SEARCH における探索失敗時に挿入の動作（ $\Theta(1)$ ）を加えているだけなので， $\Theta(n)$ 時間で実行できる．

